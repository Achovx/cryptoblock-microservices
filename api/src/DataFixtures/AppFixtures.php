<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    const DEFAULT_USER = ['username' => 'cryptoTest', 'email' => 'cryptoTest@esgi.fr', 'password' => 'password'];
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $fake = Factory::create();

        $defaultUser = new User();
        $passHash = $this->encoder->encodePassword($defaultUser, self::DEFAULT_USER['password']);

        $defaultUser->setUsername(self::DEFAULT_USER['username'])
            ->setEmail(self::DEFAULT_USER['email'])
            ->setPassword($passHash);

        $manager->persist($defaultUser);


        for ($u = 0; $u < 10; ++$u) {
            $user = new User();

            $passHash = $this->encoder->encodePassword($user, 'password');

            $user->setUsername($fake->userName);
            $user->setEmail($fake->email);
            $user->setPassword($passHash);

            $manager->persist($user);

            for ($c=0; $c < 3; $c++) {
                $category = new Category();
                $category->setName($fake->text(25));

                $manager->persist($category);

                for ($a=0; $a < random_int(5, 15); $a++) {
                    $article = new Article();
                    $article->setCategory($category);
                    $article->setAuthor($user);
                    $article->setContent($fake->text(300));
                    $article->setName($fake->text(50));
    
                    $manager->persist($article);

                    for ($s=0; $s < random_int(1, 10); $s++) {
                        $comment = new Comment();
                        $comment->setArticle($article);
                        $comment->setAuthor($user);
                        $comment->setContent($fake->text(random_int(5, 50)));
                        
                        $manager->persist($comment);
                    }
                }
            }
        }

        $manager->flush();
    }
}
