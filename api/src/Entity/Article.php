<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ArticleRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Timestampable;
use App\Controller\ArticleUpdatedAt;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 * @ApiResource(
 *      collectionOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"article_read"}}
 *          },
 *          "post"
 *      },
 *      itemOperations={
 *          "get"={
 *              "normalization_context"={"groups"={"article_details_read"}}
 *          },
 *          "put",
 *          "patch",
 *          "delete",
 *          "put_updated_at"={
 *              "method" = "PUT",
 *              "path" = "/articles/{id}/updated-at",
 *              "controller" = ArticleUpdatedAt::class
 *          }
 *      },
 *      subresourceOperations={
 *          "api_categories_articles_get_subresource"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"category_articles_read"}}
 *          }
 *      }
 * )
 */
class Article
{
    use ResourceId;
    use Timestampable;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"article_read", "user_details_read", "article_details_read", "category_details_read", "comments_details_read", "category_articles_read"})
     */
    private string $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"article_read", "user_details_read", "article_details_read", "category_details_read", "comments_details_read", "category_articles_read"})
     */
    private string $content;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="articles", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"article_read", "article_details_read", "category_details_read", "category_articles_read"})
     */
    private userInterface $author;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="articles", fetch="EAGER")
     * @Groups({"article_read", "article_details_read"})
     */
    private $Category;

    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="Article")
     * @Groups({"article_details_read"})
     */
    private Collection $comments;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->comments = new ArrayCollection();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getAuthor(): userInterface
    {
        return $this->author;
    }

    public function setAuthor(userInterface $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->Category;
    }

    public function setCategory(?Category $Category): self
    {
        $this->Category = $Category;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setArticle($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getArticle() === $this) {
                $comment->setArticle(null);
            }
        }

        return $this;
    }
}
