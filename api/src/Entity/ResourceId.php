<?php 

declare(strict_types=1);

namespace App\Entity;

trait ResourceId
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"category_read", "category_details_read", "user_read", "user_details_read", "article_read", "article_details_read", "comments_read", "comments_details_read", "category_articles_read"})
     */
    private int $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}