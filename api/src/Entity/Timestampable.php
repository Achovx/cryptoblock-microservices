<?php 

declare(strict_types=1);

namespace App\Entity;

trait Timestampable
{
    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"category_read", "category_details_read", "user_read", "user_details_read", "article_read", "article_details_read", "comments_read", "comments_details_read", "category_articles_read"})
     */
    private \DateTimeInterface $createdAt;
    
    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"category_read", "category_details_read", "user_read", "user_details_read", "article_read", "article_details_read", "comments_read", "comments_details_read", "category_articles_read"})
     */
    private ?\DateTimeInterface $updatedAt;

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}