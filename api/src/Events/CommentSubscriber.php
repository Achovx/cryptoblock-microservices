<?php

declare(strict_types=1);

namespace App\Events;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Authorizations\CommentAuthorizationChecker;
use App\Entity\Comment;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class CommentSubscriber implements EventSubscriberInterface
{
    private array $methodNotAllowed = [
        Request::METHOD_POST,
        Request::METHOD_GET
    ];
    private CommentAuthorizationChecker $commentAuthorizationChecker;

    public function __construct(CommentAuthorizationChecker $commentAuthorizationChecker)
    {
        $this->commentAuthorizationChecker = $commentAuthorizationChecker;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['check', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function check(ViewEvent $event): void
    {
        $comment = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($comment instanceof Comment &&
            !in_array($method, $this->methodNotAllowed, true)
        ) {
            $this->commentAuthorizationChecker->check($comment, $method);
            $comment->setUpdatedAt(new \DateTimeImmutable());
        }
    }
}