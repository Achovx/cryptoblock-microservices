import Vue from 'vue/dist/vue.js'
import App from './App.vue'
import VueRouter from 'vue-router'

import Home from './pages/Home.vue'
import Blog from './pages/Blog/Blog.vue'
import Category from './pages/Blog/Category.vue'
import Article from './pages/Blog/Article.vue'
import Account from './pages/Account/Account.vue'
import ChangePwd from './pages/Account/ChangePwd.vue'
import Login from './pages/Account/Login.vue'
import SignUp from './pages/Account/SignUp.vue'
import Metrics from './pages/Metrics.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: Home },
  { path: '/blog', component: Blog },
  { path: '/blog/:categoryId/:categoryName', component: Category },
  { path: '/blog/:categoryId/:categoryName/article/:id', component: Article },
  { path: '/account', component: Account },
  { path: '/account/change-password', component: ChangePwd },
  { path: '/login', component: Login },
  { path: '/sign-up', component: SignUp },
  { path: '/metrics', component: Metrics }
]

const router = new VueRouter({
  mode: "history",
  routes // short for `routes: routes`
})

Vue.config.productionTip = false

new Vue({
  router,
  render(createElement) {
      return createElement(App);
  }
}).$mount('#app');
